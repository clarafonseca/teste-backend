const { StatusCodes } = require("http-status-codes");
const { moviesService } = require("../services");
const yup = require("yup");

module.exports = {
  create: async (req, res) => {
    try {
      //Recebe movie contendo uma lista de strings, cada string representa um genero
      const schema = yup.object().shape({
        title: yup.string().required(),
        genre_name: yup.string().required(),
        language: yup.string().required(),
        tagline: yup.string().required(),
        overview: yup.string().required(),
        status: yup.string().required(),
        release_date: yup.string().required(),
      });

      await schema.validate(req.body, {
        stripUnknown: true,
      });

      const response = await moviesService.create(req.body);
      return res.status(StatusCodes.CREATED).json(response);
    } catch (error) {
      console.error(error);
      return res
        .status(error.status || StatusCodes.INTERNAL_SERVER_ERROR)
        .json(error.message);
    }
  },
  list: async (req, res) => {
    try {
      const response = await moviesService.list(req.query);

      // if (!response || response.data.length === 0) {
      //   return res.status(StatusCodes.NO_CONTENT).end();
      // }

      return res.status(StatusCodes.OK).json(response);
    } catch (error) {
      console.error(error);
      return res
        .status(error.status || StatusCodes.INTERNAL_SERVER_ERROR)
        .json(error.message);
    }
  },
  get: async (req, res) => {
    try {
      const response = await moviesService.get(req.params.id);

      return res.status(StatusCodes.OK).json(response);
    } catch (error) {
      console.error(error);
      return res
        .status(error.status || StatusCodes.INTERNAL_SERVER_ERROR)
        .json(error.message);
    }
  },
  update: async (req, res) => {
    try {
      const response = await moviesService.update(req.params.id, req.body);

      return res.status(StatusCodes.OK).json(response);
    } catch (error) {
      console.error(error);
      return res
        .status(error.status || StatusCodes.INTERNAL_SERVER_ERROR)
        .json(error.message);
    }
  },
  delete: async (req, res) => {
    try {
      const response = await moviesService.deleteOne(req.params.id);

      return res.status(StatusCodes.OK).json(response);
    } catch (error) {
      console.error(error);
      return res
        .status(error.status || StatusCodes.INTERNAL_SERVER_ERROR)
        .json(error.message);
    }
  },
};
