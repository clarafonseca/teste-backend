const { StatusCodes } = require("http-status-codes");
const { genresService } = require("../services");
const yup = require("yup");

module.exports = {
  create: async (req, res) => {
    try {
      const schema = yup.object().shape({
        genre_name: yup.string().required(),
      });

      await schema.validate(req.body, {
        stripUnknown: true,
      });

      const { genre_name } = req.body;
      const response = await genresService.create(genre_name);
      return res.status(StatusCodes.CREATED).json(response);
    } catch (error) {
      console.error(error);
      return res
        .send(error.message)
        .status(error.status || StatusCodes.INTERNAL_SERVER_ERROR)
        .json(error.message);
    }
  },
  list: async (req, res) => {
    try {
      const response = await genresService.list(req.query.perPage, req.query.page);

      if (!response || response.items.length === 0) {
        return res.status(StatusCodes.NO_CONTENT).end();
      }
      return res.status(StatusCodes.CREATED).json(response);
    }catch (error) {
      console.error(error);
      return res
        .send(error.message)
        .status(error.status || StatusCodes.INTERNAL_SERVER_ERROR)
        .json(error.message);
    }
  },
  // filter: async (req, res) => {
  //   try {
  //     const response = await genresService.filer(req.query.perPage, req.query.page);

  //     if (!response || response.items.length === 0) {
  //       return res.status(StatusCodes.NO_CONTENT).end();
  //     }
  //     return res.status(StatusCodes.CREATED).json(response);
  //   }catch (error) {
  //     console.error(error);
  //     return res
  //       .send(error.message)
  //       .status(error.status || StatusCodes.INTERNAL_SERVER_ERROR)
  //       .json(error.message);
  //   }
  // }
};
