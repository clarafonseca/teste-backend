const usersController = require("./users.controller");
const authController = require("./auth.controller");
const genreController = require("./genre.controller");
const moviesController = require("./movie.controller");
const votesController = require("./votes.controller");


module.exports = {
  usersController,
  authController,
  genreController,
  moviesController,
  votesController, 
};
