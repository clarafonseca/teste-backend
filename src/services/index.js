const authService = require("./auth");
const usersService = require("./users");
const genresService = require("./genres");
const moviesService = require("./movies");
const votesService = require("./votes");

module.exports = {
  authService,
  usersService,
  genresService,
  moviesService,
  votesService,
};
