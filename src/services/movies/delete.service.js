const yup = require('yup');
const { StatusCodes } = require('http-status-codes');
const { messages } = require("../../helpers");
const { moviesRepository } = require('../../repositories');

module.exports.deleteOne = async (id) => {
    const movie = await moviesRepository.getById(id);

    if (!movie) {
      throw {
        status: StatusCodes.NOT_FOUND,
        message: messages.notFound('movie'),
      };
    }

    return moviesRepository.destroy(movie.getDataValue('id'));
};
