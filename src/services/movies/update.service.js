const yup = require('yup');
const { StatusCodes } = require('http-status-codes');
const { messages } = require("../../helpers");
const { moviesRepository } = require('../../repositories');
const { genresRepository } = require("../../repositories");

module.exports.update = async (id, body) => {
  const movie = await moviesRepository.getById(id);

  if (!movie) {
    throw {
      status: StatusCodes.NOT_FOUND,
      messages: messages.notFound('movie'),
    };
  }

  const schema = yup.object().shape({
    title: yup.string(),
    genre_name: yup.string(),
    language: yup.string(),
    tagline: yup.string(),
    overview: yup.string(),
    status: yup.string(),
    release_date: yup.string(),
  });

  const validated = await schema.validate(body, {
    stripUnknown: true,
  });

  Object.keys(validated).forEach((key) => {
    movie.setDataValue(key, validated[key]);
  });

  const genre_name = body.genre_name;
  if (genre_name) {
    const genre = await genresRepository.get({genre_name});
    if (!genre) {
        throw {
            status: StatusCodes.NOT_FOUND,
            message: messages.notFound("genre_name"),
          };
      } 
      else {
        const genre_id = genre.id;
        movie.setDataValue('genre_id', genre_id);
      }
  }


  return moviesRepository.update(movie);
};
