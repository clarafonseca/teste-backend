const { StatusCodes } = require("http-status-codes");
const { moviesRepository } = require("../../repositories");
const { genresRepository } = require("../../repositories");
const { messages } = require("../../helpers");

module.exports.get = async (id) => {
  const movie = await moviesRepository.getById(id);

  if (!movie) {
    throw {
      status: StatusCodes.NOT_FOUND,
      msg: messages.notFound("movie"),
    };
  }

  const genre = await genresRepository.getById(movie.genre_id);
  const data = {
      id: movie.id,
      title: movie.title,
      genre: genre.dataValues.genre_name,
      language: movie.language,
      tagline: movie.tagline,
      overview: movie.overview,
      status: movie.status,
      release_date: movie.release_date,
      voteAverage: movie.voteAverage
  };
  
  return data;
};
