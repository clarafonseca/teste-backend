const { moviesRepository } = require("../../repositories");
const { genresRepository } = require("../../repositories");

module.exports.list = async (options) => {
  const { count, rows } = await moviesRepository.list({ where: options });
  const data = {...rows};
  const response = [];
  for (let i = 0; i < rows.length; i++){
    const genre_model = await genresRepository.getById(data[i].genre_id);

    response.push({
      id: data[i].id,
      title: data[i].title,
      genre: genre_model.dataValues.genre_name,
      language: data[i].language,
      tagline: data[i].tagline,
      overview: data[i].overview,
      status: data[i].status,
      release_date: data[i].release_date,
      voteAverage: data[i].voteAverage
    })
  }

  return {
    metadata: {
      total: count,
    },
    data: response,
  };
};
