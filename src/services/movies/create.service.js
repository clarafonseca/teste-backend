const { StatusCodes } = require("http-status-codes");
const { messages } = require("../../helpers");

const { moviesRepository } = require("../../repositories");
const { genresRepository } = require("../../repositories");

module.exports.create = async (body) => {
  const {
    title,
    genre_name,
    language,
    tagline,
    overview,
    status,
    release_date,
  } = body;

  const movie = await moviesRepository.get({ title, release_date });

  if (movie) {
    throw {
      status: StatusCodes.CONFLICT,
      message: messages.alreadyExists(title, release_date),
    };
  }

  const genre = await genresRepository.get({ genre_name });

  if (!genre) {
    throw {
      status: StatusCodes.NOT_FOUND,
      message: messages.notFound("genre_name"),
    };
  }
  const genre_id = genre.id;

  return moviesRepository.create({
    title,
    genre_id: genre_id,
    language,
    tagline,
    overview,
    status,
    release_date,
  });
};
