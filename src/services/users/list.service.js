const { usersRepository } = require("../../repositories");

module.exports.list = async (options) => {
  console.log(options);
  const query = {};

  if (options.query && options.query !== "") {
    query.where = { query: options.query };
  }

  const { count, rows } = await usersRepository.list(query);

  return {
    metadata: {
      total: count,
    },
    data: rows,
  };
};
