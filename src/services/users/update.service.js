const yup = require("yup");
const { StatusCodes } = require("http-status-codes");
const { messages } = require("../../helpers");
const { usersRepository } = require("../../repositories");

module.exports.update = async (req) => {
  if (req.params.id != req.session.id) {
    throw {
      status: StatusCodes.UNAUTHORIZED,
      messages: messages.authMissing(),
    };
  }
  const user = await usersRepository.getById(req.params.id);

  if (!user) {
    throw {
      status: StatusCodes.NOT_FOUND,
      messages: messages.notFound("user"),
    };
  }

  const schema = yup.object().shape({
    name: yup.string(),
    email: yup.string(),
    password: yup.string(),
  });

  const validated = await schema.validate(req.body, {
    stripUnknown: true,
  });

  Object.keys(validated).forEach((key) => {
    user.setDataValue(key, validated[key]);
  });

  return usersRepository.update(user);
};
