const { StatusCodes } = require('http-status-codes');
const { usersRepository } = require("../../repositories");
const { encryptor, messages } = require("../../helpers");

module.exports.register = async (req) => {
  const { name, email, password } = req.body;
  const check_email = await usersRepository.get({email});

  if (check_email) {
    throw {
      status: StatusCodes.CONFLICT,
      message: messages.alreadyExists("email"),
    };
  }

  let role = false;
  if (req.session && req.session.isAdmin) {
    role = true;
  } 


  return usersRepository.create({
    name,
    email,
    password,
    isAdmin: role,
  });
};