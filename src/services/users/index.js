const { list } = require("./list.service");
const { register } = require("./register.service");
const { update } = require("./update.service");
const { deleteOne } = require("./delete.service");



module.exports = {
  list,
  register,
  update,
  deleteOne,
};
