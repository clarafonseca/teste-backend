const yup = require("yup");
const { StatusCodes } = require("http-status-codes");
const { messages } = require("../../helpers");
const { usersRepository } = require("../../repositories");

module.exports.deleteOne = async (req) => {
  if (req.params.id != req.session.id) {
    throw {
      status: StatusCodes.UNAUTHORIZED,
      messages: messages.authMissing(),
    };
  }
  const user = await usersRepository.getById(req.params.id);

  if (!user) {
    throw {
      status: StatusCodes.NOT_FOUND,
      messages: messages.notFound("user"),
    };
  }
  return usersRepository.destroy(user.getDataValue('id'));
};
