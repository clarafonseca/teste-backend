const { StatusCodes } = require("http-status-codes");
const { messages } = require("../../helpers");
const { moviesRepository } = require("../../repositories");
const { votesRepository } = require("../../repositories");

module.exports.create = async (req) => {
  const movie_id = req.params.movie;
  const user_id = req.session.id;
  const movie_find = await moviesRepository.getById(movie_id);
  const vote_check = await votesRepository.get({ movie_id, user_id });

  if (vote_check) {
    throw {
      status: StatusCodes.CONFLICT,
      message: messages.alreadyExists("vote"),
    };
  }

  if (!movie_find) {
    throw {
      status: StatusCodes.NOT_FOUND,
      message: messages.notFound("movie"),
    };
  } else {
    const count = movie_find.voteCount + 1;
    const total = movie_find.voteTotal + req.body.vote;
    movie_find.setDataValue('voteCount', (count));
    movie_find.setDataValue('voteTotal', (total)); 
    movie_find.setDataValue('voteAverage', (total / count)); 
  }

  await moviesRepository.update(movie_find);


  return votesRepository.create({
    user_id: user_id,
    movie_id: movie_id,
    vote: req.body.vote,
  });
};
