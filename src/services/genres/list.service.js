const { StatusCodes } = require("http-status-codes");
const { genresRepository } = require("../../repositories");

module.exports.list = async (perPage, page) => {
   // const query = {};
    if (!perPage || Number.isNaN(perPage)) {
        perPage = 10;
      } else {
        perPage = parseInt(perPage, 10);
      }
    
      if (!page || Number.isNaN(page)) {
        page = 1;
      } else {
        page = parseInt(page, 10);
      }
    
      const genres = await genresRepository.list({
        limit: perPage,
        offset: perPage * (page - 1),
      });
    
      return {
        totalPage: Math.ceil(genres.count),
        items: genres.rows,
      };
  
    // if (options !== "") {
    //   query.where = { genre_name: options.genre_name };
    // }
  
    // const { count, rows } = await genresRepository.list(query);
  
    // return {
    //   metadata: {
    //     total: count,
    //   },
    //   data: rows,
    // };
};
