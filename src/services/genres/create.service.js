const { StatusCodes } = require("http-status-codes");
const { messages } = require("../../helpers");

const { genresRepository } = require("../../repositories");

module.exports.create = async (genre_name) => {
  const genre_check = await genresRepository.get({ genre_name });

  if (genre_check) {
    throw {
      status: StatusCodes.CONFLICT,
      message: messages.alreadyExists("genre_name"),
    };
  }

  return genresRepository.create({
      genre_name,
    });
  
};
