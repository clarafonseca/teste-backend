const { RatedMovie } = require("../models");

module.exports = {
  list: (query) => RatedMovie.findAndCountAll(query),
  getById: (id) => RatedMovie.findByPk(id),
  get: (params) => RatedMovie.findOne({ where: params }),
  create: (params) => RatedMovie.create(params),
  update: (ratedMovie) => ratedMovie.save(),
  destroy: (id) => RatedMovie.destroy({ where: { id } }),
};