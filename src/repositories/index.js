const usersRepository = require("./user.repository");
const genresRepository = require("./genre.repository");
const moviesRepository = require("./movie.repository");
const votesRepository = require("./vote.repository");


module.exports = {
  usersRepository,
  genresRepository,
  moviesRepository,
  votesRepository,
};
