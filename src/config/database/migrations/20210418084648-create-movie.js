'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('movies', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        allowNull: false,
        type: Sequelize.STRING
      },
      genre_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'genres',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      language: {
        allowNull: false,
        type: Sequelize.STRING
      },
      tagline: {
        allowNull: false,
        type: Sequelize.STRING
      },      
      overview: {
        allowNull: false,
        type: Sequelize.STRING(370)
      },
      status: {
        allowNull: false,
        type: Sequelize.STRING
      },
      release_date: {
        allowNull: false,
        type: Sequelize.STRING
      },
      vote_total: {
        allowNull: false,
        type: Sequelize.FLOAT,
        defaultValue: '0'
      },
      vote_average: {
        allowNull: false,
        type: Sequelize.FLOAT,
        defaultValue: '0'
      },
      vote_count: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: '0'
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('movies');
  }
};