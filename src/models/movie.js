'use strict';

const Genre = require('./genre');

module.exports = (sequelize, DataTypes) => {
  const Movie = sequelize.define(
    "Movie",
    {
      title: DataTypes.STRING,
      genre_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: Genre,
          key: 'id',
        },
      },
      language: DataTypes.STRING,
      tagline: DataTypes.STRING,
      overview: DataTypes.STRING,
      status: DataTypes.STRING,
      release_date: DataTypes.STRING,
      voteTotal: {
        type: DataTypes.FLOAT,
        field: "vote_total",
      },
      voteAverage: {
        type: DataTypes.FLOAT,
        field: "vote_average",
      },
      voteCount: {
        type: DataTypes.INTEGER,
        field: "vote_count",
      },
      createdAt: {
        type: DataTypes.DATE,
        field: "created_at",
      },
      updatedAt: {
        type: DataTypes.DATE,
        field: "updated_at",
      },
      deletedAt: {
        type: DataTypes.DATE,
        field: "deleted_at", 
      }
    },
    {
      tableName: "movies",
      paranoid: true,
    }
    
  );
  return Movie;
};