'use strict';

const User = require('./user');
const Movie = require('./movie');


module.exports = (sequelize, DataTypes) => {
  const RatedMovies = sequelize.define(
    "RatedMovie",
    {
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: User,
          key: 'id',
        },
      },
      movie_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: Movie,
          key: 'id',
        },
      },
      vote: DataTypes.DECIMAL(2, 1),
      createdAt: {
        type: DataTypes.DATE,
        field: "created_at",
      },
      updatedAt: {
        type: DataTypes.DATE,
        field: "updated_at",
      },
      deletedAt: {
        type: DataTypes.DATE,
        field: "deleted_at", 
      }
    },
    {
      tableName: "rated_movies",
      paranoid: true,
    }
    
  );
  return RatedMovies;
};