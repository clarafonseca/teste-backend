const router = require('express').Router();
const { genreController } = require('../controllers');
const { isAuthenticated } = require("../middlewares");
const { isAuthorized } = require("../middlewares");


router.all('*', isAuthenticated);
router.post('/', isAuthorized, genreController.create);
router.get('/', genreController.list);
// router.put('/:id', isAuthorized, genreController.update);
// router.delete('/:id', isAuthorized, genreController.delete);

module.exports.genres = router;