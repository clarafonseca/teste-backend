const { auth } = require("./auth.routes");
const { users } = require("./users.routes");
const { genres } = require(`./genres.routes`);
const { movies } = require(`./movies.routes`);
const { rate } = require(`./rating.routes`);

module.exports = {
  auth,
  users,
  genres,
  movies,
  rate,
};
