const router = require('express').Router();
 const { votesController } = require('../controllers');
const { isAuthenticated } = require("../middlewares");
const { isAuthorized } = require("../middlewares");


router.all('*', isAuthenticated);
router.post('/:movie', votesController.create);
// router.get('/', moviesController.list);
// router.get('/:id', moviesController.get);
// router.put('/:id', isAuthorized, moviesController.update);
// router.delete('/:id', isAuthorized, moviesController.delete);

module.exports.rate = router;