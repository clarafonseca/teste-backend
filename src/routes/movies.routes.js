const router = require('express').Router();
const { moviesController } = require('../controllers');
const { isAuthenticated } = require("../middlewares");
const { isAuthorized } = require("../middlewares");


router.all('*', isAuthenticated);
router.post('/', isAuthorized, moviesController.create);
router.get('/', moviesController.list);
router.get('/:id', moviesController.get);
router.put('/:id', isAuthorized, moviesController.update);
router.delete('/:id', isAuthorized, moviesController.delete);

module.exports.movies = router;