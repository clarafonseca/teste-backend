const router = require("express").Router();
const { authController } = require("../controllers");
const { isAuthorized } = require("../middlewares");
const { isAuthenticated } = require("../middlewares");

router.post("/signin", authController.signin);

module.exports.auth = router;
